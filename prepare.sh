#!/bin/bash

apt update -qq

apt install software-properties-common -y

apt-add-repository --yes --update ppa:ansible/ansible

add-apt-repository ppa:nextcloud-devs/client -y --update

apt install ansible python-apt keepassxc vim nextcloud-client git -y

mkdir -p $HOME/repositories/my

#ssh-keygen -f $HOME/.ssh/id_rsa -q -P ""

rm -f $HOME/.ssh/known_hosts

ln -s /dev/null $HOME/.ssh/known_hosts